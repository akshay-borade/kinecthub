namespace :update do
  desc "Updating status of transaction"
  task transaction_status: :environment do
    Transaction.all.each do |transaction|
      transaction.status = transaction.status.upcase
      transaction.save
    end
  end

  desc "Updating status of Distribution Overview"
  task distributions_status: :environment do
    DistributionOverview.all.each do |distribution_overview|
      distribution_overview.status = distribution_overview.status.upcase
      distribution_overview.save
    end
  end
end
