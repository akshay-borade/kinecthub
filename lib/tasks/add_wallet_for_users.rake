namespace :users do
  desc "Migrating Default Users"
  task add_wallet: :environment do
    User.all.each do |user|
      unless user.key.present?
        loop do
          user.key = SecureRandom.uuid
          break if user.valid?
        end
        user.save
        puts "#{user.email} key: " + user.key + " generated"
        unless user.wallet_address.present?
          body = RestClient.post(ENV['wallet_base_url'] + 'addUserWallet', { id: user.key }, { authorization: "Basic #{ENV['authorizeCode']}" })
          data = JSON.parse(body)
          user.wallet_address = data["response"]["walletAddress"]
          puts "#{user.email} wallet: " + user.wallet_address + " generated"
          user.save
        end
      end
    end
  end
end
