def initialize_user(role_name, email, obj_name = nil)
  user = User.new({
                    email: email,
                    password: '123456789',
                    uid: SecureRandom.hex(6),
                    fullname: obj_name || Faker::Name.name,
                    address1: Faker::Address.street_address,
                    address2: Faker::Address.secondary_address,
                    suburb: Faker::Address.city,
                    state: Faker::Address.state,
                    postal_code: Faker::Address.postcode,
                    country: Faker::Address.country,
                    phone: Faker::PhoneNumber.phone_number
                  })
  user.save
  user.add_role(Role::NAMES[role_name.to_sym])
  user
end

namespace :default_users do
  desc "Migrating Default Users"
  task seed: :environment do
    puts "Creating clinic"
    initialize_user('clinic', "clinic@yopmail.com", "AIDS Clinic (Active)")

    puts "Creating Lab"
    initialize_user('lab', 'lab@yopmail.com')
    puts "Lab created"
  end

  desc "Migrating KinectAid Users"
  task kinectaid: :environment do
    initialize_user('aid', 'aid@yopmail.com')
  end
end
