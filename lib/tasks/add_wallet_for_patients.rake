namespace :patients do
  desc "Migrating Default Patients"
  task add_wallet: :environment do
    Patient.all.each do |patient|
      unless patient.key.present?
        loop do
          patient.key = SecureRandom.hex
          break if patient.valid?
        end
        patient.save
        puts "#{patient.email} key: " + patient.key + " generated"
        unless patient.wallet_address.present?
          body = RestClient.post(ENV['wallet_base_url'] + 'addUserWallet', { id: patient.key }, { authorization: "Basic #{ENV['authorizeCode']}" })
          data = JSON.parse(body)
          patient.wallet_address = data["response"]["walletAddress"]
          puts "#{patient.email} wallet: " + patient.wallet_address + " generated"
          patient.save
        end
      end
    end
  end
end
