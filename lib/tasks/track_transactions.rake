namespace :track do
  desc "Migrating Default Patients"
  task transactions: :environment do
    transactions = Transaction.where(status: Transaction::STATUS[:pending])
    web3 = Web3::Eth::Rpc.new host: 'ropsten.infura.io',
                              port: 443,
                              connect_options: {
                                open_timeout: 20,
                                read_timeout: 140,
                                use_ssl: true
                              }
    transactions.each do |transaction|
      TransactionWorker.new.perform(transaction, web3)
    end
  end
end
