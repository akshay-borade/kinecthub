# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_16_165658) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "consultations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.date "assessment_date"
    t.integer "created_by"
    t.boolean "completed", default: false
    t.boolean "token_assigned", default: false
    t.integer "disease", default: 0
    t.index ["patient_id"], name: "index_consultations_on_patient_id"
    t.index ["user_id"], name: "index_consultations_on_user_id"
  end

  create_table "diagnoses", force: :cascade do |t|
    t.boolean "hiv", default: false
    t.boolean "co_existing_hep_b", default: false
    t.text "notes"
    t.bigint "consultation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "questions", default: {}
    t.string "diagnose_type"
    t.index ["consultation_id"], name: "index_diagnoses_on_consultation_id"
  end

  create_table "distribution_overviews", force: :cascade do |t|
    t.string "token"
    t.string "transaction_id"
    t.string "status"
    t.bigint "user_id"
    t.bigint "patient_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "transaction_type"
    t.string "wallet_address"
    t.string "description"
    t.index ["patient_id"], name: "index_distribution_overviews_on_patient_id"
    t.index ["user_id"], name: "index_distribution_overviews_on_user_id"
  end

  create_table "examinations", force: :cascade do |t|
    t.string "height"
    t.string "weight"
    t.string "temperature"
    t.string "blood_sugar_level"
    t.string "avpu"
    t.string "pupils"
    t.string "pulse"
    t.integer "heart_rate"
    t.string "heart_rate_regularity"
    t.string "respiration"
    t.integer "breaths_per_minute"
    t.string "spo2"
    t.string "trachea"
    t.string "blood_pressure"
    t.boolean "lung_diagram_available", default: false
    t.json "blood_pressure_systolic"
    t.json "blood_pressure_diastolic"
    t.string "lung_diagram_storage_location"
    t.text "lung_diagram_notes"
    t.boolean "abdomen_diagram_available", default: false
    t.string "abdomen_diagram_storage_location"
    t.text "abdomen_diagram_notes"
    t.boolean "body_diagram_available", default: false
    t.string "body_diagram_storage_location"
    t.text "body_diagram_notes"
    t.boolean "photo_available", default: false
    t.string "photo_storage_location"
    t.bigint "consultation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "questions", default: {}
    t.index ["consultation_id"], name: "index_examinations_on_consultation_id"
  end

  create_table "general_notes", force: :cascade do |t|
    t.text "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "consultation_id"
  end

  create_table "histories", force: :cascade do |t|
    t.boolean "caesarian_section_pre_1992", default: false
    t.boolean "mother_with_heaptitis_c_before_patient_born", default: false
    t.boolean "household_member_with_heaptitis_c", default: false
    t.boolean "unwell_at_borth_cause_uncertain", default: false
    t.boolean "tattoo_branding_scarification_or_body_piercing", default: false
    t.boolean "received_a_blood_transfusion_prior_to_1992", default: false
    t.boolean "received_an_organ_transfusion_prior_to_1992", default: false
    t.boolean "received_clotting_factor_concentrates_produced_prior_to_1987", default: false
    t.boolean "ever_on_long_term__1_month_hemodialysis", default: false
    t.boolean "currently_inject_or_inhaled_drugs_not_prescribed_by_a_doctor", default: false
    t.boolean "ever_injected_or_inhaled_drugs_not_prescribed_by_a_doctor_even_", default: false
    t.boolean "are_needle_exchange_services_available_in_your_vicinity", default: false
    t.boolean "currently_use_non_injectable_inhaled_drugs_not_prescribed_by_a_", default: false
    t.boolean "ever_use_non_injectable_inhaled_drugs_not_prescribed_by_a_docto", default: false
    t.boolean "employed_in_a_medical_or_dental_field_involving_direct_contact_", default: false
    t.boolean "ever_incarcerated", default: false
    t.boolean "has_aids_hep_b_c", default: false
    t.boolean "has_hepatitis_b", default: false
    t.boolean "ever_had_one_or_more_of_these_sexually_transmitted_diseases_syp", default: false
    t.boolean "sexual_partner_or_patient_have_been_diagnosed_or_treated_for_an", default: false
    t.boolean "current_or_any_previous_sexual_partner_has_hepatitis_c", default: false
    t.boolean "is_the_patient_a_man_who_ever_even_if_only_once_had_sex_with_an", default: false
    t.boolean "sexual_partner_is_in_a_high_risk_group_for_hepatitis_c_blood_bl", default: false
    t.boolean "ever_had_contact_with_a_person_who_had_hepatitis_c", default: false
    t.boolean "occupation_with_increased_hepatitis_c_risk_now_or_past", default: false
    t.boolean "symptomatic", default: false
    t.boolean "clinical_signs", default: false
    t.boolean "jaundiced", default: false
    t.boolean "hospitalized_from_hepatitis", default: false
    t.boolean "pregnant", default: false
    t.boolean "smoking_current_or_in_past", default: false
    t.boolean "alcohol_consumption_current_or_in_past", default: false
    t.boolean "diagnosed_or_suspected_alcoholism", default: false
    t.string "sexual_orientation"
    t.boolean "multiple_sexual_partners_more_than_1_per_year_anytime_in_last_2", default: false
    t.integer "number_of_sexual_partners_last_6_months"
    t.integer "number_of_sexual_partners_last_12_months"
    t.bigint "consultation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "questions", default: {}
    t.index ["consultation_id"], name: "index_histories_on_consultation_id"
  end

  create_table "laboratory_requests", force: :cascade do |t|
    t.boolean "rcc", default: false
    t.boolean "wcc", default: false
    t.boolean "esr", default: false
    t.boolean "crp", default: false
    t.boolean "inr", default: false
    t.boolean "alt_sgpt", default: false
    t.boolean "ast_sgot", default: false
    t.boolean "billrubin", default: false
    t.boolean "albumin", default: false
    t.boolean "pcr_viral_load", default: false
    t.boolean "p24_only_test_ag", default: false
    t.boolean "fourth_g_antigen_antibody_test", default: false
    t.boolean "rapid_test", default: false
    t.boolean "western_blot_test", default: false
    t.boolean "ultrasound", default: false
    t.boolean "ct", default: false
    t.boolean "mri", default: false
    t.boolean "hepatomegaly", default: false
    t.boolean "cirrhosis", default: false
    t.boolean "splenic_enlargement", default: false
    t.boolean "ascites", default: false
    t.boolean "abnormal_blood_flow", default: false
    t.bigint "consultation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "completed"
    t.jsonb "questions", default: {}
    t.index ["consultation_id"], name: "index_laboratory_requests_on_consultation_id"
  end

  create_table "laboratory_results", force: :cascade do |t|
    t.json "rcc"
    t.json "wcc"
    t.json "esr"
    t.json "crp"
    t.json "inr"
    t.json "alt_sgpt"
    t.json "ast_sgot"
    t.json "billrubin"
    t.json "albumin"
    t.string "pcr_viral_load"
    t.string "p24_only_test_ag"
    t.string "fourth_g_antigen_antibody_test"
    t.string "rapid_test"
    t.string "western_blot_test"
    t.date "date_of_imaging"
    t.string "ultrasound"
    t.string "ct"
    t.string "mri"
    t.string "other"
    t.json "hepatomegaly"
    t.json "cirrhosis"
    t.json "splenic_enlargement"
    t.json "ascites"
    t.json "abnormal_blood_flow"
    t.bigint "consultation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "laboratory_request_id"
    t.date "date_of_test"
    t.jsonb "questions", default: {}
    t.index ["consultation_id"], name: "index_laboratory_results_on_consultation_id"
  end

  create_table "patients", force: :cascade do |t|
    t.string "uid"
    t.string "first_name"
    t.string "middle_name"
    t.string "last_name"
    t.string "email"
    t.text "address"
    t.string "suburb"
    t.string "country"
    t.string "state"
    t.string "postal_code"
    t.string "phone"
    t.date "date_of_birth"
    t.string "gender", default: "male"
    t.string "ethnicity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "key"
    t.string "wallet_address"
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "otp_secret_key"
    t.index ["reset_password_token"], name: "index_patients_on_reset_password_token", unique: true
  end

  create_table "roles", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "transactions", force: :cascade do |t|
    t.integer "number_of_patients"
    t.decimal "number_of_tokens"
    t.integer "assigned_patients"
    t.text "patients_ids", default: [], array: true
    t.integer "user_id"
    t.integer "clinic_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tx_hash"
  end

  create_table "treatments", force: :cascade do |t|
    t.string "medication1_regime1"
    t.text "medication1_regime1_treatment_notes"
    t.string "medication2_regime2"
    t.text "medication2_regime2_treatment_notes"
    t.bigint "consultation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "questions", default: {}
    t.index ["consultation_id"], name: "index_treatments_on_consultation_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "fullname"
    t.string "uid"
    t.text "address1"
    t.text "address2"
    t.string "suburb"
    t.string "state"
    t.string "postal_code"
    t.string "country"
    t.string "phone"
    t.integer "clinic_id"
    t.integer "doctor_id"
    t.string "key"
    t.string "wallet_address"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

end
