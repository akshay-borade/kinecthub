class AddCompletedToConsultations < ActiveRecord::Migration[5.2]
  def change
    add_column :consultations, :completed, :boolean, default: false
    add_column :consultations, :token_assigned, :boolean, default: false
  end
end
