class AddFieldsInUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :fullname, :string
    add_column :users, :uid, :string
    add_column :users, :address1, :text
    add_column :users, :address2, :text
    add_column :users, :suburb, :string
    add_column :users, :state, :string
    add_column :users, :postal_code, :string
    add_column :users, :country, :string
    add_column :users, :phone, :string
  end
end
