class RemoveParentIdFromUser < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :parent_id, :integer
    add_column :users, :clinic_id, :integer
  end
end
