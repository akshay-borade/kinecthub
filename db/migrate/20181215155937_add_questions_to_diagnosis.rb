class AddQuestionsToDiagnosis < ActiveRecord::Migration[5.2]
  def change
    add_column :diagnoses, :questions, :jsonb, default: {}
    add_column :diagnoses, :diagnose_type, :string
  end
end
