class AddQuestionsToHistory < ActiveRecord::Migration[5.2]
  def change
    add_column :histories, :questions, :jsonb, default: {}
  end
end
