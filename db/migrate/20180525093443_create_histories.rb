class CreateHistories < ActiveRecord::Migration[5.2]
  def change
    create_table :histories do |t|
      t.boolean :caesarian_section_pre_1992, default: false
      t.boolean :mother_with_heaptitis_c_before_patient_born, default: false
      t.boolean :household_member_with_heaptitis_c, default: false
      t.boolean :unwell_at_borth_cause_uncertain, default: false
      t.boolean :tattoo_branding_scarification_or_body_piercing, default: false
      t.boolean :received_a_blood_transfusion_prior_to_1992, default: false
      t.boolean :received_an_organ_transfusion_prior_to_1992, default: false
      t.boolean :received_clotting_factor_concentrates_produced_prior_to_1987, default: false
      t.boolean :ever_on_long_term__1_month_hemodialysis, default: false
      t.boolean :currently_inject_or_inhaled_drugs_not_prescribed_by_a_doctor, default: false
      t.boolean :ever_injected_or_inhaled_drugs_not_prescribed_by_a_doctor_even_if_only_once_or_a_few_times, default: false
      t.boolean :are_needle_exchange_services_available_in_your_vicinity, default: false
      t.boolean :currently_use_non_injectable_inhaled_drugs_not_prescribed_by_a_doctor, default: false
      t.boolean :ever_use_non_injectable_inhaled_drugs_not_prescribed_by_a_doctor_even_if_only_once_or_a_few_times, default: false
      t.boolean :employed_in_a_medical_or_dental_field_involving_direct_contact_with_human_blood, default: false
      t.boolean :ever_incarcerated, default: false
      t.boolean :has_aids_hep_b_c, default: false
      t.boolean :has_hepatitis_b, default: false
      t.boolean :ever_had_one_or_more_of_these_sexually_transmitted_diseases_syphilis_gonnorhoea_chlamydia_genital_herpes, default: false
      t.boolean :sexual_partner_or_patient_have_been_diagnosed_or_treated_for_an_std, default: false
      t.boolean :current_or_any_previous_sexual_partner_has_hepatitis_c, default: false
      t.boolean :is_the_patient_a_man_who_ever_even_if_only_once_had_sex_with_another_man_msm, default: false
      t.boolean :sexual_partner_is_in_a_high_risk_group_for_hepatitis_c_blood_blood_product_transfusion_iv, default: false
      t.boolean :ever_had_contact_with_a_person_who_had_hepatitis_c, default: false
      t.boolean :occupation_with_increased_hepatitis_c_risk_now_or_past, default: false
      t.boolean :symptomatic, default: false
      t.boolean :clinical_signs, default: false
      t.boolean :jaundiced, default: false
      t.boolean :hospitalized_from_hepatitis, default: false
      t.boolean :pregnant, default: false
      t.boolean :smoking_current_or_in_past, default: false
      t.boolean :alcohol_consumption_current_or_in_past, default: false
      t.boolean :diagnosed_or_suspected_alcoholism, default: false
      t.string :sexual_orientation
      t.boolean :multiple_sexual_partners_more_than_1_per_year_anytime_in_last_20_years, default: false
      t.integer :number_of_sexual_partners_last_6_months
      t.integer :number_of_sexual_partners_last_12_months
      t.belongs_to :consultation, index: true
      t.timestamps
    end
  end
end
