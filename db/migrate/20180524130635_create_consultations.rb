class CreateConsultations < ActiveRecord::Migration[5.2]
  def change
    create_table :consultations do |t|
      t.belongs_to :user, index: true
      t.belongs_to :patient, index: true
      t.timestamps
    end
  end
end
