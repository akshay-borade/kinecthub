class AddFieldCreatedByInConsultations < ActiveRecord::Migration[5.2]
  def change
    add_column :consultations, :created_by, :integer
  end
end
