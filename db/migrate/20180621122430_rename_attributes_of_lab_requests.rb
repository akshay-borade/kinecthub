class RenameAttributesOfLabRequests < ActiveRecord::Migration[5.2]
  def change
    rename_column :laboratory_requests, :alt_sgot, :ast_sgot
    rename_column :laboratory_results, :billirubin, :billrubin
    rename_column :laboratory_requests, :fourth_generation_antigen_antibody, :fourth_g_antigen_antibody_test
    rename_column :laboratory_requests, :rapid_tests, :rapid_test
    rename_column :laboratory_requests, :western_blot_tests, :western_blot_test
    rename_column :laboratory_requests, :abnormal_blood_flow_shunting, :abnormal_blood_flow
    add_column :laboratory_requests, :completed, :boolean
  end
end
