class CreateLaboratoryResults < ActiveRecord::Migration[5.2]
  def change
    create_table :laboratory_results do |t|
      t.json :rcc
      t.json :wcc
      t.json :esr
      t.json :crp
      t.json :inr
      t.json :alt_sgpt
      t.json :ast_sgot
      t.json :billirubin
      t.json :albumin
      t.string :pcr_viral_load
      t.string :p24_only_test_ag
      t.string :fourth_g_antigen_antibody_test
      t.string :rapid_test
      t.string :western_blot_test
      t.date :date_of_imaging
      t.string :ultrasound
      t.string :ct
      t.string :mri
      t.string :other
      t.json :hepatomegaly
      t.json :cirrhosis
      t.json :splenic_enlargement
      t.json :ascites
      t.json :abnormal_blood_flow
      t.belongs_to :consultation, index: true
      t.timestamps
    end
  end
end
