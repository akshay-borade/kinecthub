class AddLaboratoryRequestIdToLaboratoryResult < ActiveRecord::Migration[5.2]
  def change
    add_column :laboratory_results, :laboratory_request_id, :integer, index: true
    add_column :laboratory_results, :date_of_test, :date
  end
end
