class AddAssessmentDateToConsultations < ActiveRecord::Migration[5.2]
  def change
    add_column :consultations, :assessment_date, :date
  end
end
