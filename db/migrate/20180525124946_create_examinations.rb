class CreateExaminations < ActiveRecord::Migration[5.2]
  def change
    create_table :examinations do |t|
      t.string :height
      t.string :weight
      t.string :temperature
      t.string :blood_sugar_level
      t.string :avpu
      t.string :pupils
      t.string :pulse
      t.integer :heart_rate
      t.string :heart_rate_regularity
      t.string :respiration
      t.integer :breaths_per_minute
      t.string :spo2
      t.string :trachea
      t.string :blood_pressure
      t.boolean :lung_diagram_available, default: false
      t.json :blood_pressure_systolic
      t.json :blood_pressure_diastolic
      t.string :lung_diagram_storage_location
      t.text :lung_diagram_notes
      t.boolean :abdomen_diagram_available, default: false
      t.string :abdomen_diagram_storage_location
      t.text :abdomen_diagram_notes
      t.boolean :body_diagram_available, default: false
      t.string :body_diagram_storage_location
      t.text :body_diagram_notes
      t.boolean :photo_available, default: false
      t.string :photo_storage_location
      t.belongs_to :consultation, index: true
      t.timestamps
    end
  end
end
