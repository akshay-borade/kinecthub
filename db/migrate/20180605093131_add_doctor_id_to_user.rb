class AddDoctorIdToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :doctor_id, :integer
  end
end
