class AddDiseaseToConsultation < ActiveRecord::Migration[5.2]
  def change
    add_column :consultations, :disease, :integer, default: 0
  end
end
