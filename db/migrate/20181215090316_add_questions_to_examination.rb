class AddQuestionsToExamination < ActiveRecord::Migration[5.2]
  def change
    add_column :examinations, :questions, :jsonb, default: {}
  end
end
