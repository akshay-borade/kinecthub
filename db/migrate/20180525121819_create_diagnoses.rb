class CreateDiagnoses < ActiveRecord::Migration[5.2]
  def change
    create_table :diagnoses do |t|
      t.boolean :hiv, default: false
      t.boolean :co_existing_hep_b, default: false
      t.text :notes
      t.belongs_to :consultation, index: true
      t.timestamps
    end
  end
end
