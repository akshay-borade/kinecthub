class AddFieldsToDistributionOverviews < ActiveRecord::Migration[5.2]
  def change
    add_column :distribution_overviews, :transaction_type, :string
    add_column :distribution_overviews, :wallet_address, :string
  end
end
