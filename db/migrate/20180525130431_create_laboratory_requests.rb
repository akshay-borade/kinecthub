class CreateLaboratoryRequests < ActiveRecord::Migration[5.2]
  def change
    create_table :laboratory_requests do |t|
      t.boolean :rcc, default: false
      t.boolean :wcc, default: false
      t.boolean :esr, default: false
      t.boolean :crp, default: false
      t.boolean :inr, default: false
      t.boolean :alt_sgpt, default: false
      t.boolean :alt_sgot, default: false
      t.boolean :billrubin, default: false
      t.boolean :albumin, default: false
      t.boolean :pcr_viral_load, default: false
      t.boolean :p24_only_test_ag, default: false
      t.boolean :fourth_generation_antigen_antibody, default: false
      t.boolean :rapid_tests, default: false
      t.boolean :western_blot_tests, default: false
      t.boolean :ultrasound, default: false
      t.boolean :ct, default: false
      t.boolean :mri, default: false
      t.boolean :hepatomegaly, default: false
      t.boolean :cirrhosis, default: false
      t.boolean :splenic_enlargement, default: false
      t.boolean :ascites, default: false
      t.boolean :abnormal_blood_flow_shunting, default: false
      t.belongs_to :consultation, index: true
      t.timestamps
    end
  end
end
