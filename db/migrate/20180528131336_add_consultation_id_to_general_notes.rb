class AddConsultationIdToGeneralNotes < ActiveRecord::Migration[5.2]
  def change
    add_column :general_notes, :consultation_id, :integer, index: true
  end
end
