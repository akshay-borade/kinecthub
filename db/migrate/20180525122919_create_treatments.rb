class CreateTreatments < ActiveRecord::Migration[5.2]
  def change
    create_table :treatments do |t|
      t.string :medication1_regime1
      t.text :medication1_regime1_treatment_notes
      t.string :medication2_regime2
      t.text :medication2_regime2_treatment_notes
      t.belongs_to :consultation, index: true
      t.timestamps
    end
  end
end
