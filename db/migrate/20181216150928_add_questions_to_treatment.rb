class AddQuestionsToTreatment < ActiveRecord::Migration[5.2]
  def change
    add_column :treatments, :questions, :jsonb, default: {}
  end
end
