class RemoveIndexFromEmailOnPatient < ActiveRecord::Migration[5.2]
  def change
    remove_index(:patients, name: 'index_patients_on_email')
  end
end
