class AddTxHashToTransactions < ActiveRecord::Migration[5.2]
  def change
    add_column :transactions, :tx_hash, :string
  end
end
