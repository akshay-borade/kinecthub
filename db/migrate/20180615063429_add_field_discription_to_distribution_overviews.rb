class AddFieldDiscriptionToDistributionOverviews < ActiveRecord::Migration[5.2]
  def change
    add_column :distribution_overviews, :description, :string
  end
end
