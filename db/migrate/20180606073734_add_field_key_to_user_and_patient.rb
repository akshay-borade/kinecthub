class AddFieldKeyToUserAndPatient < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :key, :string
    add_column :patients, :key, :string
  end
end
