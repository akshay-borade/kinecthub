class CreatePatients < ActiveRecord::Migration[5.2]
  def change
    create_table :patients do |t|
      t.string :uid
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :email
      t.text :address
      t.string :suburb
      t.string :country
      t.string :state
      t.string :postal_code
      t.string :phone
      t.date :date_of_birth
      t.string :gender, default: "male"
      t.string :ethnicity

      t.timestamps
    end
  end
end
