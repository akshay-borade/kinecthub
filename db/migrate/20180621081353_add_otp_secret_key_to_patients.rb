class AddOtpSecretKeyToPatients < ActiveRecord::Migration[5.2]
  def change
    add_column :patients, :otp_secret_key, :string
  end
end
