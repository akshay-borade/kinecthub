class AddQuestionsToLaboratoryRequestAndResult < ActiveRecord::Migration[5.2]
  def change
    add_column :laboratory_requests, :questions, :jsonb, default: {}
    add_column :laboratory_results, :questions, :jsonb, default: {}
  end
end
