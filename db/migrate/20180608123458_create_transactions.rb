class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.integer :number_of_patients
      t.decimal :number_of_tokens
      t.integer :assigned_patients
      t.text :patients_ids, array: true, default: []
      t.integer :user_id
      t.integer :clinic_id
      t.string :status

      t.timestamps
    end
  end
end
