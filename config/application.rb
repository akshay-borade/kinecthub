require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module KinectHub
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.assets.paths << Rails.root.join("app", "assets", "fonts")
    config.filter_parameters << :password

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration can go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded after loading
    # the framework and any gems in your application.
  end
end

# Raven.configure do |config|
#   config.dsn = 'https://92f96af39efb49829fe4672829ceb7da:51374fe491a04a3aa579fda41159bd61@sentry.io/1230833'
#   config.sanitize_fields = Rails.application.config.filter_parameters.map(&:to_s)
# end
