bind  "unix:///var/www/kinect.com/shared/tmp/sockets/puma.sock"
pidfile "/var/www/kinect.com/shared/tmp/pids/puma.pid"
state_path "/var/www/kinect.com/shared/tmp/sockets/puma.state"
directory "/var/www/kinect.com/current"

threads_count = 5
threads threads_count, threads_count

environment "production"

plugin :tmp_restart

workers 2
