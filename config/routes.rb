Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  devise_for :users, :skip => [:registrations]

  as :user do
    get '/users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
    put '/users' => 'devise/registrations#update', :as => 'user_registration'
  end

  devise_for :patients, :skip => [:registrations], controllers: { sessions: 'patients/sessions' }

  as :patient do
    get "/patients/edit" => "devise/registrations#edit", :as => :edit_patient_registration
    put "/patients" => "devise/registrations#update", :as => :patient_registration
  end

  root to: 'dashboard#index'

  resources :dashboard, only: [:index] do
    collection do
      get 'states'
      post 'transaction_callback'
    end
  end

  resources :patients, except: :destroy do
    collection do
      post :send_otp
    end
    resources :consultations do
      resources :histories, only: [:create, :update]
      resources :treatments, only: [:create, :update]
      resources :examinations
      resources :diagnoses
      resources :general_notes
      resources :laboratory_requests, except:  :index
      resources :laboratory_results
      member do
        patch 'update_provider'
        patch 'update_disease'
      end
    end
  end

  resources :users, only: [] do
    collection do
      get 'user_balance'
      post 'add_tokens'
    end
    member do
      post 'send_reset_password_token'
    end
  end

  resources :clinics, only: [:index, :show] do
    member do
      get 'outcomes'
    end
    resources :transactions, only: [:create]
  end
  resources :transactions, only: [:index]

  get '/laboratory_patients', to: 'laboratory_requests#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :healthcare_professionals, except: [:show, :destroy]

  get '*path' => redirect('/')
end
