class PatientMailer < ApplicationMailer
  def welcome_email patient, otp
    @patient = patient
    @otp = otp
    @url = "#{root_url}users/sign_in?phone=#{patient.phone}#patient"
    mail(to: patient.email, subject: 'Welcome to KinectHub')
  end
end
