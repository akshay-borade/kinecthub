class TransactionWorker
  include Sidekiq::Worker

  def perform(transaction, web3)
    response = web3.eth.getTransactionReceipt(transaction.tx_hash)
    if response.present?
      status = response.status.gsub('0x', '').to_i
      if status
        transaction.update(status: Transaction::STATUS[:completed])
        channel =
          if transaction.user.present?
            "transactions_#{transaction.user.id}_channel"
          elsif transaction.patient.present?
            "transactions_#{transaction.patient.id}_channel"
          end
        if channel.present?
          ActionCable.server.broadcast channel,
                                       transaction: transaction,
                                       user: transaction.user
        end
      else
        transaction.update(status: Transaction::STATUS[:failed])
      end
    end
  end
end
