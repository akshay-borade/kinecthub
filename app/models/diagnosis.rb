class Diagnosis < ApplicationRecord
  belongs_to :consultation
  DIAGNOSIS_TYPES = {
    chest_infection: "Chest infection",
    suspected_tb: "Suspected TB",
    new_tuberculosis: "New Tuberculosis",
    recurrent_tuberculosis: "Recurrent Tuberculosis",
    extra_pulmonary_tuberculosis: "Extra pulmonary Tuberculosis",
    mdr_tuberculosis: "MDR Tuberculosis",
    other: "Other"
  }
end
