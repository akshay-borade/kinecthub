class Examination < ApplicationRecord
  TB_QUESTIONS = {
    cough_2_weeks: "Cough 2 weeks",
    weight_loss: "Weight loss",
    haemoptysis_blood_in_your_spit: "Haemoptysis ( blood in your spit)",
    fever: "Fever",
    night_sweats: "Night sweats",
    fatigue: "Fatigue",
    persistent_headache: "Persistent Headache",
    reduced_appetite: "Reduced appetite"
  }

  belongs_to :consultation
  validates :height, :weight, :temperature, numericality: true, allow_blank: true

  validates_presence_of :lung_diagram_storage_location, if: lambda { self.lung_diagram_available.present? }
  validates_presence_of :photo_storage_location, if: lambda { self.photo_available.present? }
  validates_presence_of :abdomen_diagram_storage_location, if: lambda { self.abdomen_diagram_available.present? }
  validates_presence_of :body_diagram_storage_location, if: lambda { self.body_diagram_available.present? }
end
