class User < ApplicationRecord
  rolify
  include GenerateToken
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  before_create :generate_secure_key
  after_create :add_wallet

  has_many :consultations
  has_many :patients, through: :consultations

  has_many :transactions, dependent: :destroy

  validates :email, :uid, :fullname, :phone, presence: true

  validates :uid, uniqueness: { message: "This ID has already been taken" }

  # has_many :doctors, :class_name => 'User', :foreign_key => 'clinic_id'
  has_many :healthcare_professionals, :class_name => 'User', :foreign_key => 'clinic_id'

  belongs_to :clinic, :class_name => 'User', optional: true
  # belongs_to :doctor, :class_name => 'User', optional: true

  has_many :distribution_overviews

  attr_accessor :phonenumber

  def clinician?
    roles.last.name == Role::NAMES[:clinician]
  end

  def doctor?
    roles.last.name == Role::NAMES[:doctor]
  end

  def lab?
    roles.last.name == Role::NAMES[:lab]
  end

  def clinic?
    roles.last.name == Role::NAMES[:clinic]
  end

  def aid?
    roles.last.name == Role::NAMES[:aid]
  end

  def specialist?
    roles.last.name == Role::NAMES[:specialist]
  end

  def nurse?
    roles.last.name == Role::NAMES[:nurse]
  end

  def all_patients
    # Patient.joins(:users).where("users.id = ?", doctors.joins(:patients).ids)
    patients_ids = []
    healthcare_professionals.joins(:patients).uniq.each do |hp|
      patients_ids << hp.patients.ids
    end
    patients_ids.flatten!
    Patient.where(id: patients_ids)
  end
end
