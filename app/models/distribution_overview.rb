class DistributionOverview < ApplicationRecord
  belongs_to :user, optional: true
  belongs_to :patient, optional: true

  STATUS = {
    initiated: 'INITIATED',
    not_initiated: 'NOT_INITIATED',
    completed: 'COMPLETED',
    failed: 'FAILED'
  }
end
