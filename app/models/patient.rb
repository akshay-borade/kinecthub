class Patient < ApplicationRecord
  include GenerateToken
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :consultations, dependent: :destroy
  has_many :users, through: :consultations
  has_many :distribution_overviews, dependent: :destroy

  validates :uid, :first_name, :last_name, :phone, :date_of_birth, presence: true

  validates :uid, uniqueness: { message: "This ID has already been taken" }
  validates :phone, uniqueness: { message: "This number is already registered with us." }
  validates :email, uniqueness: { message: "This email has already been taken" }, allow_blank: true
  validate :patient_username

  # attr_accessor :doctor
  attr_accessor :username

  before_create :generate_secure_key
  after_create :add_wallet

  has_one_time_password

  def fullname
    self.first_name + ' ' + self.last_name
  end

  def lab_result_available
    LaboratoryResult.where(consultation_id: consultations.ids).present? ? "Yes" : "No"
  end

  def medication_prescribed
    Treatment.where(consultation_id: consultations.ids).present? ? "Yes" : "No"
  end

  def send_text message
    client = Twilio::REST::Client.new(ENV['twilio_account_sid'], ENV['twilio_auth_token'])
    begin
      client.messages.create(
        from: ENV['twilio_phone_number'],
        to: self.phone,
        body: message
      )
    rescue => e
      puts e
      true
    end
  end

  def patient_username
    if errors.present? && errors[:phone].present?
      errors.add(:username, errors[:phone][0])
    end
  end

  private

  def email_required?
    false
  end
end
