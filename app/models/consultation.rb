class Consultation < ApplicationRecord
  belongs_to :user, inverse_of: :consultations
  belongs_to :patient, inverse_of: :consultations

  enum disease: [ :tuberculosis, :hiv ]

  delegate :fullname, to: :patient, prefix: true
  delegate :fullname, to: :user, prefix: true
  delegate :uid, to: :patient
  delegate :id, to: :laboratory_request, prefix: true
  delegate :fullname, to: :user, prefix: true

  has_one :history
  has_one :treatment

  has_one :examination
  has_one :diagnosis
  has_one :general_note
  has_one :laboratory_request
  has_one :laboratory_result

  after_update :assign_kct

  def assign_kct
    # KCT amount allocated to Healthcare Provider (10 Tokens)
    if self.completed && !self.token_assigned
      user = self.user
      distribution = user.distribution_overviews.create(description: "Consultation complete")
      data = { users: ([{ key: user.key, token: ENV["healthCareProviderToken"], overviewID: distribution.id }]).to_json }
      RestClient.post(ENV['wallet_base_url'] + 'distributeToken', data, { authorization: "Basic #{ENV['authorizeCode']}" })
      self.token_assigned = true
      self.save
    end
  end
end
