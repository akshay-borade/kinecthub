class Transaction < ApplicationRecord
  belongs_to :user

  after_update :update_distribution_overviews, if: lambda { status.eql?(STATUS[:completed]) }

  STATUS = {
    pending: 'PENDING',
    completed: 'COMPLETED',
    failed: 'FAILED',
  }

  def update_distribution_overviews
    user.distribution_overviews.create(token: self.number_of_tokens.to_i, status: STATUS[:completed],
                                       transaction_type: "SENT", wallet_address: user.wallet_address,
                                       description: I18n.t('token_sent'), transaction_id: tx_hash)
  end
end
