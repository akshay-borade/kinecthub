module GenerateToken
  extend ActiveSupport::Concern

  def generate_secure_key
    loop do
      key = SecureRandom.uuid
      break self.key = key unless self.class.exists?(key: key)
    end
  end

  def add_wallet
    body = RestClient.post(ENV['wallet_base_url'] + 'addUserWallet', { id: self.key }, { authorization: "Basic #{ENV['authorizeCode']}" })
    data = JSON.parse(body)
    self.wallet_address = data["response"]["walletAddress"]
    self.save
  end
end
