module UpdateLabRequest
  extend ActiveSupport::Concern

  def update_laboratory_result request_model, result_model
    tests = ['rcc', 'wcc', 'esr', 'crp', 'inr', 'alt_agpt', 'ast_sgpt', 'billrubin', 'albumin',
             'pcr_viral_load', 'p24_only_test_ag', 'fourth_g_antigen_antibody_test', 'rapid_test', 'western_blot_test',
             'ultrasound', 'ct', 'mri', 'hepatomegaly', 'cirrhosis', 'splenic_enlargement', 'ascites', 'abnormal_blood_flow']
    lab_request = request_model.attributes
    flag = true
    lab_request.each do |key, value|
      next unless value && tests.include?(key)
      begin
        data = JSON.parse(result_model[key])
        next if data["result"].present? || data["upper_limit"].present? || data["date"].present?
        flag = false
        break
      rescue StandardError
        next if result_model[key].present?
        flag = false
        break
      end
    end
    flag
  end
end
