class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
    if user.doctor?
      can [:read, :create, :update], Patient
      cannot [:read, :create], User
      # can :update, User, doctor_id: user.id
    end

    if user.clinician? || user.specialist? || user.nurse?
      cannot :manage, User
      can :manage, Patient
    end

    if user.lab?
      cannot :manage, User
      cannot :manage, Patient
    end

    if user.clinic?
      cannot :manage, Patient
      can :manage, User, :clinic_id => user.id
    end

    if user.aid?
      can :read, User
      cannot :manage, Patient
    end
  end
end
