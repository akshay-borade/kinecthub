class History < ApplicationRecord
  belongs_to :consultation
  attr_accessor :assessment_date
  attr_accessor :user_id

  # validates :sexual_orientation, presence: true
  validates_presence_of :number_of_sexual_partners_last_6_months,
                        :number_of_sexual_partners_last_12_months, if: lambda { self.multiple_sexual_partners_more_than_1_per_year_anytime_in_last_2.present? }

  TB_QUESTIONS = {
    previous_sputum_test: "Previous sputum test",
    previously_diagnosed_with_tb: "Previously diagnosed with TB",
    previously_treated_for_tb: "Previously treated for TB",
    previous_tb_exposure: "Previous TB Exposure",
    time_in_prison: "Time in Prison",
    family_members_with_tb: "Family members with TB",
    previously_diagnosed_with_hiv: "Previously diagnosed with HIV"
  }

end
