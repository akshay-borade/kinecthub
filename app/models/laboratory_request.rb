class LaboratoryRequest < ApplicationRecord
  include UpdateLabRequest

  after_create :assign_kct
  before_update :check_lab_results
  belongs_to :consultation
  has_one :laboratory_result

  TB_QUESTIONS = {
    sputum_examination: "Sputum examination ( gene xpert))",
    cbc: "CBC ( complete blood count)",
    hiv: "HIV",
    random_sugar: "Random sugar",
    chest_xray: "Chest xray",
    hbsag: "Hbsag",
    hcv: "HCV",
    dst: "DST( culture& drug sensitivity test)"
  }

  def assign_kct
    # KCT amount allocated to Healthcare Provider (10 Tokens)
    user = self.consultation.user
    distribution = user.distribution_overviews.create(description: "Lab tests ordered")
    data = { users: ([{ key: user.key, token: ENV["healthCareProviderToken"], overviewID: distribution.id }]).to_json }
    RestClient.post(ENV['wallet_base_url'] + 'distributeToken', data, { authorization: "Basic #{ENV['authorizeCode']}" })
  end

  def check_lab_results
    if self.laboratory_result
      flag = update_laboratory_result(self, self.laboratory_result)
      self.completed = flag
    end
  end
end
