class LaboratoryResult < ApplicationRecord
  after_create :assign_kct
  after_create :update_lab_request
  after_update :update_lab_request

  include UpdateLabRequest

  belongs_to :consultation
  belongs_to :laboratory_request

  def assign_kct
    data = { users: [] }
    # KCT amount allocated to Laboratory (5 Tokens)
    laboratory = Role.find_by_name("Lab").users.first
    laboratory_distribution = laboratory.distribution_overviews.create(description: "Lab test results entered into EHR")
    data[:users].push({
                        key: laboratory.key,
                        token: ENV["laboratoryToken"],
                        overviewID: laboratory_distribution.id
                      })
    # KCT amount allocated to Patient (5 Tokens)
    patient = self.consultation.patient
    patient_distribution = patient.distribution_overviews.create(description: "Patient visits lab, undertakes tests")
    data[:users].push({
                        key: patient.key,
                        token: ENV["patientToken"],
                        overviewID: patient_distribution.id
                      })
    data[:users] = data[:users].to_json
    RestClient.post(ENV['wallet_base_url'] + 'distributeToken', data, { authorization: "Basic #{ENV['authorizeCode']}" })
  end

  def update_lab_request
    flag = update_laboratory_result(self.laboratory_request, self)
    self.laboratory_request.update(completed: flag)
  end
end
