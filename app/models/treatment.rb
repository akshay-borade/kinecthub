class Treatment < ApplicationRecord
  belongs_to :consultation
  validates_presence_of :medication1_regime1, :medication2_regime2, if: lambda { self.consultation.diagnosis.try(:hiv) }

  TB_QUESTIONS = {
    amoxycillin: 'amoxycillin',
    co_trimoxazole: 'co-trimoxazole',
    isoniazid_5mg_kg: 'Isoniazid (5mg/kg)',
    rifampin_10mg_kg: 'Rifampin (10mg/kg)',
    pyrazinamide_25_mg_kg: 'Pyrazinamide ( 25 mg/kg)',
    ethambutol_15mg_kg: 'Ethambutol (15mg/kg)',
    treptomycin: 'Streptomycin',
    other_drug: "Other drug"
  }

  OPTION_VALUES = {
    two_months: "2 Months",
    four_month: "4 Months",
    other: "Other"
  }

  DEFAULT_OPTION = {
    amoxycillin: 'other',
    co_trimoxazole: 'other',
  }

end
