function checkMetamaskWeb3(){
  if (typeof web3 !== 'undefined') {
    window.web3 = new Web3(web3.currentProvider);
    let account = web3.eth.accounts[0];
    if (!account) {
      toastr.warning("You must install & Login to MetaMask in order to make transactions. After you have installed MetaMask, you must refresh this page to continue.");
      return false;
    } else {
      return true;
    }
    return true;
  } else {
    toastr.warning("You must install & Login to MetaMask in order to make transactions. After you have installed MetaMask, you must refresh this page to continue.");
    return false;
  }
}


function checkWeb3(){
  var web3 = new Web3();
  web3.setProvider(new web3.providers.HttpProvider("https://ropsten.infura.io/UL0KvyQG8wXoNAxpc5VA"));
  if (typeof web3 !== 'undefined') {
    window.web3 = web3;
    return true;
  }
}
