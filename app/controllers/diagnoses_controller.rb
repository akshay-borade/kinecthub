class DiagnosesController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :set_consultation, only: [:create, :update]

  def create
    if @consultation.diagnosis.present?
      @diagnosis = @consultation.diagnosis
    else
      @diagnosis = Diagnosis.new(diagnosis_params)
      @diagnosis.consultation = @consultation
      @diagnosis.save
      render json: { hiv: @diagnosis.hiv }
    end
  end

  def update
    @consultation.diagnosis.update(diagnosis_params)
    render json: { hiv: @consultation.diagnosis.hiv }
  end

  private

  def set_consultation
    @consultation = Consultation.find(params[:consultation_id])
  end

  def diagnosis_params
    params.require(:diagnosis).permit(:hiv, :co_existing_hep_b, :notes, :diagnose_type)
  end
end
