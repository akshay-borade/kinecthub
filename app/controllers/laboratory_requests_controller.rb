class LaboratoryRequestsController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :find_patient_and_consultant, except: :index

  def index
    if current_user.lab?
      laboratory_requests = LaboratoryRequest.includes(:consultation)
      if params[:commit]
        laboratory_requests = laboratory_requests
                              .joins(consultation: [:patient, :user])
                              .where("lower(patients.uid) LIKE ? AND (lower(patients.first_name) LIKE ? OR lower(patients.last_name) LIKE ?) AND lower(users.fullname) LIKE ?",
                                     "%#{params[:patient_id]}%".downcase, "%#{params[:patient_name]}%".downcase, "%#{params[:patient_name]}%".downcase, "%#{params[:doctor_name]}%".downcase)
        laboratory_requests = laboratory_requests.where("consultations.assessment_date = ?", params[:assessment_date].to_date) if params[:assessment_date].present?
      end
      @laboratory_requests = laboratory_requests.includes(consultation: [:patient, :user]).page(params[:page]).per(20)
    else
      redirect_to patients_path, flash: { alert: t('denied_permission') }
    end
  end

  def create
    if @consultation.laboratory_request.present?
      @laboratory_request = @consultation.laboratory_request
    else
      @laboratory_request = LaboratoryRequest.new(lab_request_params)
      @laboratory_request.consultation = @consultation
      unless @laboratory_request.save
        flash[:alert] = "Errors while creating laboratory request"
        redirect_to patient_consultation_path(@patient, @consultation)
      else
        @laboratory_result = @consultation.laboratory_result || @consultation.build_laboratory_result(laboratory_request: @laboratory_request)
        render json: { lab_request: @consultation.laboratory_request.attributes }
      end
    end
  end

  def update
    unless @consultation.laboratory_request.update(lab_request_params)
      flash[:alert] = 'Errors while updating consultation laboratory request'
      redirect_to patient_consultation_path(@patient, @consultation)
    else
      render json: { lab_request: @consultation.laboratory_request.attributes }
    end
  end

  private

  def lab_request_params
    params.require(:laboratory_request).permit(:rcc, :wcc, :esr, :crp, :inr, :alt_sgpt, :ast_sgot, :billrubin, :albumin, :pcr_viral_load,
                                               :p24_only_test_ag, :fourth_g_antigen_antibody_test, :rapid_test, :western_blot_test,
                                               :ultrasound, :ct, :mri, :hepatomegaly, :cirrhosis, :splenic_enlargement, :ascites, :abnormal_blood_flow, questions: {})
  end
end
