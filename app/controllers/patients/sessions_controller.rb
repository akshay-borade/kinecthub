class Patients::SessionsController < Devise::SessionsController
  def create
    patient = Patient.find_by_phone(params[:patient][:phone])
    if patient.present?
      if patient.authenticate_otp(params[:patient][:password], drift: 300)
        set_flash_message(:notice, :signed_in) if is_navigational_format?
        sign_in(:patient, patient)
        if !session[:return_to].blank?
          redirect_to session[:return_to]
          session[:return_to] = nil
        else
          respond_with patient, :location => after_sign_in_path_for(patient)
        end
      else
        flash[:alert] = "Your OTP is expired, please request another OTP"
        redirect_to new_user_session_url + "?phone=#{patient.phone}" + '#patient'
      end
    else
      flash[:alert] = "Patient is not available with this phone number"
      redirect_to new_user_session_url + "#patient"
    end
  end
end
