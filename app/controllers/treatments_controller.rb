class TreatmentsController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :find_patient_and_consultant

  def create
    if @consultation.treatment.present?
      @treatment = @consultation.treatment
    else
      @treatment = @consultation.build_treatment(treatment_params)
      if @treatment.save
        render json: { data: true }
      else
        flash[:alert] = "Errors while creating treatment"
        redirect_to patient_consultation_path(@patient, @consultation)
      end
    end
  end

  def update
    if @consultation.treatment.update(treatment_params)
      render json: { data: true }
    else
      flash[:alert] = 'Errors while updating consultation treatment'
      redirect_to patient_consultation_path(@patient, @consultation)
    end
  end

  private

  def treatment_params
    params.require(:treatment).permit(:medication1_regime1, :medication1_regime1_treatment_notes, :medication2_regime2, :medication2_regime2_treatment_notes, questions: {})
  end
end
