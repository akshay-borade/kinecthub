class HistoriesController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :find_patient_and_consultant

  def create
    if @consultation.history.present?
      @history = @consultation.history
    else
      @history = History.new(history_params)
      @history.consultation = @consultation
      unless @history.save
        flash[:alert] = "Errors while creating history"
        redirect_to patient_consultation_path(@patient, @consultation)
      else
        _update_assessment_date
        render json: { data: true }
      end
    end
  end

  def update
    unless @consultation.history.update(history_params)
      flash[:alert] = 'Errors while updating consultation history'
      redirect_to patient_consultation_path(@patient, @consultation)
    else
      _update_assessment_date
      render json: { data: true }
    end
  end

  private

  def _update_assessment_date
    assessment_date = params[:history][:assessment_date]
    @consultation.update(assessment_date: assessment_date.to_date) if assessment_date.present? && !assessment_date.eql?("Invalid Date")
  end

  def history_params
    params.require(:history).permit(:caesarian_section_pre_1992, :mother_with_heaptitis_c_before_patient_born, :household_member_with_heaptitis_c,
                                    :unwell_at_borth_cause_uncertain, :tattoo_branding_scarification_or_body_piercing, :received_a_blood_transfusion_prior_to_1992,
                                    :received_an_organ_transfusion_prior_to_1992, :received_clotting_factor_concentrates_produced_prior_to_1987,
                                    :ever_on_long_term__1_month_hemodialysis, :currently_inject_or_inhaled_drugs_not_prescribed_by_a_doctor,
                                    :ever_injected_or_inhaled_drugs_not_prescribed_by_a_doctor_even_, :are_needle_exchange_services_available_in_your_vicinity,
                                    :currently_use_non_injectable_inhaled_drugs_not_prescribed_by_a_, :ever_use_non_injectable_inhaled_drugs_not_prescribed_by_a_docto,
                                    :employed_in_a_medical_or_dental_field_involving_direct_contact_, :ever_incarcerated, :has_aids_hep_b_c, :has_hepatitis_b,
                                    :ever_had_one_or_more_of_these_sexually_transmitted_diseases_syp, :sexual_partner_or_patient_have_been_diagnosed_or_treated_for_an,
                                    :current_or_any_previous_sexual_partner_has_hepatitis_c, :is_the_patient_a_man_who_ever_even_if_only_once_had_sex_with_an,
                                    :sexual_partner_is_in_a_high_risk_group_for_hepatitis_c_blood_bl, :ever_had_contact_with_a_person_who_had_hepatitis_c,
                                    :occupation_with_increased_hepatitis_c_risk_now_or_past, :symptomatic, :clinical_signs, :jaundiced, :hospitalized_from_hepatitis,
                                    :pregnant, :smoking_current_or_in_past, :alcohol_consumption_current_or_in_past, :diagnosed_or_suspected_alcoholism,
                                    :sexual_orientation, :multiple_sexual_partners_more_than_1_per_year_anytime_in_last_2, :number_of_sexual_partners_last_6_months,
                                    :number_of_sexual_partners_last_12_months, questions: {})
  end
end
