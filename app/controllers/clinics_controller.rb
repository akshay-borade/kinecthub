class ClinicsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_all_clinics, only: [:index, :show]

  def index
    gon.contract_abi = ContractSetting[:contractABI]
    gon.contract_addresss = ContractSetting[:contractAddress]
  end

  def show
    @clinic = @clinics.find_by_id(params[:id])
    @transaction = @clinic.transactions.new
    @transaction_count = current_user.distribution_overviews.where.not(status: DistributionOverview::STATUS[:completed]).count
    gon.contract_abi = ContractSetting[:contractABI]
    gon.contract_addresss = ContractSetting[:contractAddress]
  end

  def outcomes
    transactions = current_user.transactions.where(clinic_id: params[:id], status: Transaction::STATUS[:completed])
    p_ids = transactions.collect(&:patients_ids)
    @transaction_data = {}
    @transaction_data[:patients] = transactions.sum(:number_of_patients)
    @transaction_data[:tokens] = transactions.sum(:number_of_tokens).to_i
    @patients = Patient.where(id: p_ids.flatten.uniq).includes(:consultations).page(params[:page]).per(20)
    redirect_to clinic_path(params[:id]) unless @patients.present?
  end

  private

  def find_all_clinics
    @clinics = User.with_role(Role::NAMES[:clinic])
  end
end
