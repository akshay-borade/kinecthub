class ApplicationController < ActionController::Base
  before_action :check_user
  before_action :set_raven_context

  def validate_date(date)
    Date.strptime(date, "%m-%d-%Y") rescue date
  end

  def fetch_all_patients
    if current_user.doctor?
      current_user.patients
    else
      current_user.clinic.all_patients
    end
  end

  # Find patient and consultant record
  def find_patient_and_consultant
    @patient =
      if current_user.lab?
        Patient.find_by(id: params[:patient_id])
      else
        fetch_all_patients.find_by(id: params[:patient_id])
      end
    consultation_id = params[:consultation_id] || params[:id]
    @consultation = @patient.consultations.find_by_id(consultation_id) if @patient.present?
    unless @patient.present? && @consultation.present?
      flash[:alert] = "No Such consultation found"
      redirect_to patients_path
    end
  end

  private

  def after_sign_out_path_for(*)
    new_user_session_path
  end

  def set_raven_context
    Raven.user_context(id: session[:current_user_id]) # or anything else in session
    Raven.extra_context(params: params.to_unsafe_h, url: request.url)
  end

  protected

  def after_sign_in_path_for(resource)
    if resource.class.name.eql?('User')
      if current_user.aid?
        clinics_path
      else
        root_path
      end
    else
      patient_path(resource)
    end
  end

  def check_user
    if current_patient && controller_name != "patients"
      flash.clear
      patient_path(current_patient) && return
    end
  end

  def authenticate_user_or_patient
    if current_patient
      redirect_to patient_path(current_patient)
    else
      authenticate_user!
    end
  end
end
