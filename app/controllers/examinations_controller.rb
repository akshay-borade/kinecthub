class ExaminationsController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :set_consultation, only: [:create, :update]

  def create
    if @consultation.examination.present?
      @examination = @consultation.examination
    else
      @examination = Examination.new(examination_params)
      @examination.consultation = @consultation
      render json: { data: @examination.save }
    end
  end

  def update
    render json: { data: @consultation.examination.update(examination_params) }
  end

  private

  def set_consultation
    @consultation = Consultation.find_by_id(params[:consultation_id])
  end

  def examination_params
    params.require(:examination).permit(:height, :weight, :temperature, :blood_sugar_level, :avpu, :pupils, :pulse, :heart_rate, :heart_rate_regularity, :respiration, :breaths_per_minute, :spo2, :blood_pressure_systolic, :blood_pressure_diastolic, :lung_diagram_available, :lung_diagram_storage_location, :lung_diagram_notes, :abdomen_diagram_available, :abdomen_diagram_storage_location, :abdomen_diagram_notes, :body_diagram_available, :body_diagram_storage_location, :body_diagram_notes, :photo_available, :photo_storage_location, :consultation_id, questions: {})
  end
end
