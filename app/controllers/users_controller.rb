class UsersController < ApplicationController
  before_action :authenticate_user_or_patient, except: :send_reset_password_token

  def user_balance
  end

  def add_tokens
    distribution = current_user.distribution_overviews.create(status: DistributionOverview::STATUS[:initiated], description: "Tokens requested")
    data = { walletAddress: params[:walletAddress], token: params[:token].to_i, overviewID: distribution.id }
    RestClient.post(ENV['wallet_base_url'] + 'addToken', data, { authorization: "Basic #{ENV['authorizeCode']}" })
  end

  def send_reset_password_token
    user = User.find_by_id(params[:id])
    user.send_reset_password_instructions if user.present?
  end
end
