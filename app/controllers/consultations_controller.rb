class ConsultationsController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :find_patient_and_consultant, only: :show
  before_action :find_consultations, except: [:show, :update]

  def index
    if params[:commit]
      assessment_date = params[:assessment_date]
      @consultations = @consultations.where("assessment_date = ?", assessment_date.to_date) if assessment_date.present?
      @consultations = @consultations.where("lower(users.fullname) LIKE ?", "%#{params[:doctor_name]}%".downcase)
    end
  end

  def create
    consultation = @patient.consultations.create(user_id: params[:doctor_id], assessment_date: params[:assessment_date])
    if consultation.persisted?
      redirect_to patient_consultation_path(@patient, consultation)
    else
      redirect_to patient_consultations_path(@patient), flash: { alert: "Errors occured while creating consultation" }
    end
  end

  # Show details for patient and doctor consultation
  def show
    @history = @consultation.history || @consultation.build_history
    @treatment = @consultation.treatment || @consultation.build_treatment
    @laboratory_request = @consultation.laboratory_request || @consultation.build_laboratory_request
    if @laboratory_request.persisted?
      gon.laboratory_request = @laboratory_request
      @laboratory_result = @consultation.laboratory_result || @consultation.build_laboratory_result(laboratory_request: @laboratory_request)
    else
      @laboratory_result = LaboratoryResult.new
    end
    @examination = @consultation.examination.present? ? @consultation.examination : Examination.new({ consultation_id: @consultation.id })
    @diagnosis = @consultation.diagnosis.present? ? @consultation.diagnosis : Diagnosis.new({ consultation_id: @consultation.id })
    @general_note = @consultation.general_note.present? ? @consultation.general_note : GeneralNote.new({ consultation_id: @consultation.id })
  end

  def update
    if consultation_complete_params.present?
      consultation = Consultation.find(consultation_complete_params[:id])
      consultation.update(completed: consultation_complete_params[:completed].eql?("true"))
      redirect_to patient_consultation_path(consultation.patient, consultation)
    end
  end

  # Update healthcare provider for patient
  def update_provider
    consultation = @consultations.where(id: params[:id]).first
    consultation.update(user_id: params[:user_id])
    redirect_to patient_consultation_path(@patient, consultation), flash: { notice: "Your Healthcare Provider is updated." }
  end

  def update_disease
    consultation = @consultations.where(id: params[:id]).first
    consultation.update(disease: params[:disease])
    redirect_to patient_consultation_path(@patient, consultation), flash: { notice: "Disease has been updated." }
  end

  private

  def find_consultations
    @patient = find_patient
    @consultations = @patient.consultations.joins(:user).page(params[:page]).per(20) if @patient.present?
  end

  def consultation_complete_params
    params.require(:consultation).permit(:id, :completed)
  end

  def find_patient
    if current_user.lab?
      Patient.find_by_id(params[:patient_id])
    else
      patients = fetch_all_patients
      patients.find_by_id(params[:patient_id])
    end
  end
end
