class TransactionsController < ApplicationController
  before_action :authenticate_user!
  before_action :retrieve_remaining_patients, only: :create

  def index
    gon.contract_abi = ContractSetting[:contractABI]
    gon.contract_addresss = ContractSetting[:contractAddress]
    @distribution_overviews = current_user.distribution_overviews.where(status: DistributionOverview::STATUS[:completed]).page(params[:page]).per(20)
  end

  def create
    transaction = current_user.transactions.new(transaction_params)
    requested_patients = params[:transaction][:number_of_patients].to_i
    if requested_patients <= @patients.count
      transaction.patients_ids = @patients.first(requested_patients).map { |p| p.id }
      transaction.assigned_patients = requested_patients
      transaction.status = Transaction::STATUS[:pending]
      if transaction.save
        redirect_to clinic_path(@clinic.id)
      end
    else
      flash[:alert] = "Requested no. of patients are not available"
      redirect_to clinic_path(@clinic.id) and return
    end
  end

  private

  def transaction_params
    params.require(:transaction).permit(:number_of_patients, :number_of_tokens, :user_id, :clinic_id, :assigned_patients, :patients_ids, :status, :tx_hash)
  end

  def retrieve_remaining_patients
    clinics = User.with_role(Role::NAMES[:clinic])
    @clinic = clinics.find_by_id(params[:clinic_id])
    p_ids = current_user.transactions.collect(&:patients_ids).flatten
    @patients = @clinic.all_patients.where.not(id: p_ids)
  end
end
