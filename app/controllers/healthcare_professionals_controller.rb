class HealthcareProfessionalsController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :find_healthcare_professional, only: [:edit, :update]

  def index
    @healthcare_professionals = current_user.healthcare_professionals.page(params[:page]).per(20)
  end

  def new
    @healthcare_professional = current_user.healthcare_professionals.new
    unless can? :create, @healthcare_professional
      redirect_to root_path, flash: { alert: t('denied_permission') }
    end
  end

  def create
    @healthcare_professional = current_user.healthcare_professionals.new(healthcare_professional_params)
    @healthcare_professional.password = SecureRandom.hex(10)
    if @healthcare_professional.save
      @healthcare_professional.add_role(params[:user][:roles])
      @healthcare_professional.send_reset_password_instructions
      redirect_to edit_healthcare_professional_path(@healthcare_professional), flash: { notice: 'Healthcare professional created!' }
    else
      render :new
    end
  end

  def edit
    @role = @healthcare_professional.roles.last.name
    unless can? :update, @healthcare_professional
      redirect_to root_path, flash: { alert: t('denied_permission') }
    end
  end

  def update
    if can? :update, @healthcare_professional
      if @healthcare_professional.update(healthcare_professional_params)
        @healthcare_professional.add_role(params[:user][:roles])
        redirect_to edit_healthcare_professional_path(@healthcare_professional), flash: { notice: 'Healthcare professional updated!' }
      else
        render :edit
      end
    else
      redirect_to root_path
    end
  end

  private

  def find_healthcare_professional
    @healthcare_professional = User.find_by(id: params[:id])
    redirect_to new_healthcare_professional_path, flash: { alert: 'No Healthcare Professional found' } unless @healthcare_professional
  end

  def healthcare_professional_params
    params.require(:user).permit(:uid, :fullname, :email, :address1, :suburb, :postal_code, :phone, :state, :country)
  end
end
