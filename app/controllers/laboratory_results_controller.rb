class LaboratoryResultsController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :find_consultant, only: [:create, :update]

  def create
    if @consultation.laboratory_result.present?
      @laboratory_result = @consultation.laboratory_result
    else
      @laboratory_result = LaboratoryResult.new(lab_result_params)
      @laboratory_result.consultation = @consultation
      @laboratory_result.laboratory_request_id = @consultation.laboratory_request_id
      unless @laboratory_result.save
        flash[:alert] = "Errors while creating laboratory result"
        redirect_to patient_consultation_path(@consultation.patient, @consultation)
      end
      redirect_to patient_consultation_path(@consultation.patient, @consultation), flash: { notice: "Laboratory Result created" } if current_user.lab?
    end
  end

  def update
    unless @consultation.laboratory_result.update(lab_result_params)
      flash[:alert] = 'Errors while updating consultation laboratory result'
      redirect_to patient_consultation_path(@consultation.patient, @consultation)
    end
    redirect_to patient_consultation_path(@consultation.patient, @consultation), flash: { notice: "Laboratory Result Updated" } if current_user.lab?
  end

  private

  def find_consultant
    @consultation = Consultation.find_by(id: params[:consultation_id])
    redirection_path unless @consultation
  end

  def redirection_path
    current_user.lab? ? laboratory_patients_path : patients_path
  end

  def lab_result_params
    params.require(:laboratory_result).permit(:rcc, :wcc, :esr, :crp, :erp, :inr, :alt_sgpt, :ast_sgot, :billrubin, :albumin,
                                              :pcr_viral_load, :p24_only_test_ag, :fourth_g_antigen_antibody_test, :rapid_test,
                                              :western_blot_test, :date_of_imaging, :ultrasound, :ct, :mri, :hepatomegaly,
                                              :cirrhosis, :splenic_enlargement, :ascites, :abnormal_blood_flow, :date_of_test, questions: {})
  end
end
