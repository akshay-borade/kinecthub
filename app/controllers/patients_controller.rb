class PatientsController < ApplicationController
  before_action :authenticate_user!, except: [:show, :send_otp]
  before_action :authenticate_patient!, only: :show
  before_action :find_patient, only: [:edit, :update]

  # Listing existing patient and search on existing patient
  def index
    if can? :read, Patient
      patients = fetch_all_patients
      if params[:commit]
        patients = patients.joins(:consultations).where('lower(uid) LIKE ? AND lower(first_name) LIKE ? AND lower(last_name) LIKE ?',
                                                        "%#{params[:patient_id]}%".downcase, "%#{params[:first_name]}%".downcase, "%#{params[:last_name]}%".downcase)
      end
      patients = patients.includes(:consultations).uniq.reverse
      @patients = Kaminari.paginate_array(patients).page(params[:page]).per(20)
    else
      redirect_to root_path, flash: { alert: t('denied_permission') }
    end
  end

  # New patient instance from current_user(doctor)
  def new
    @patient = current_user.patients.new
    unless can? :create, @patient
      redirect_to root_path, flash: { alert: t('denied_permission') }
    end
  end

  # Created patient for doctor and redirect to consultation when clicked on "Create Consultation"
  def create
    @patient = current_user.patients.new(patient_params)
    @patient.password = SecureRandom.hex(10)
    if @patient.save
      flash[:notice] = "Patient created successfully"
      PatientMailer.welcome_email(@patient, nil).deliver if @patient.email.present?
      @patient.send_text "Your account with username #{@patient.phone} is successfully registered on KinectHub. Please sign in here #{root_url}users/sign_in?phone=#{@patient.phone}#patient"
      redirect_patient_path
    else
      render :new
    end
  end

  # edit path for patient
  def edit
    redirect_to root_path, flash: { alert: t('denied_permission') } unless can? :update, @patient
  end

  # Updated patient details and redirect to consultation when clicked on "Create Consultation"
  def update
    if @patient.update(patient_params)
      flash[:notice] = "Patient updated successfully"
      redirect_patient_path
    else
      flash[:alert] = "Errors while updating patient"
      redirect_to edit_patient_path(@patient)
    end
  end

  def show
    gon.contract_abi = ContractSetting[:contractABI]
    gon.contract_addresss = ContractSetting[:contractAddress]
  end

  def send_otp
    patient = Patient.find_by_phone(params[:phone])

    if patient.present?
      patient.send_text "KinectHub siginin OTP is #{patient.otp_code} and it is valid for 5 min."
      PatientMailer.welcome_email(patient, patient.otp_code).deliver if patient.email.present?
      render json: { success: true, message: "OTP sent to your phone number" }
    else
      render json: { success: false, message: "Patient is not available with this phone number." }
    end
  end

  private

  # Permit patient params
  def patient_params
    params[:patient][:date_of_birth] = validate_date(params[:patient][:date_of_birth])
    params.require(:patient).permit(:uid, :first_name, :middle_name, :country, :state, :email,
                                    :last_name, :address, :suburb, :postal_code, :phone, :date_of_birth, :gender, :ethnicity)
  end

  # Redirect to consultation when clicked on "Create Consultation" from patient create/edit view
  def redirect_patient_path
    if params[:commit] == "Create Consultation"
      consultation = @patient.consultations.find_by(user_id: current_user.id)
      redirect_to patient_consultation_path(@patient, consultation)
    else
      redirect_to edit_patient_path(@patient)
    end
  end

  # Find patient record
  def find_patient
    patients = fetch_all_patients
    @patient = patients.where(id: params[:id]).first
    redirect_to new_patient_path, flash: { alert: 'No patient found' } unless @patient.present?
  end
end
