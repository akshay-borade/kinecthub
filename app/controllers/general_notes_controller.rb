class GeneralNotesController < ApplicationController
  before_action :authenticate_user_or_patient
  before_action :set_consultation, only: [:create, :update]

  def create
    if @consultation.general_note.present?
      @general_note = @consultation.general_note
    else
      @general_note = GeneralNote.new(general_note_params)
      @general_note.consultation = @consultation
      @general_note.save
      flash[:notice] = "Details submitted successfully"
      redirect_to patient_consultation_path(@consultation.patient, @consultation)
    end
  end

  def update
    if @consultation.general_note.update(general_note_params)
      flash[:notice] = "Details submitted successfully"
      redirect_to patient_consultation_path(@consultation.patient, @consultation)
    end
  end

  private

  def set_consultation
    @consultation = Consultation.find_by(id: params[:consultation_id])
  end

  def general_note_params
    params.require(:general_note).permit(:note)
  end
end
