class DashboardController < ApplicationController
  before_action :authenticate_user_or_patient, except: :transaction_callback
  protect_from_forgery with: :null_session, only: :transaction_callback

  def index
    if current_user.aid?
      redirect_to clinics_path
    end
  end

  def states
    if params[:country].present?
      render json: { states: CS.states(params[:country]).sort }
    else
      render json: { states: [] }
    end
  end

  def transaction_callback
    if distribution_params[:overviewID].present?
      overview = DistributionOverview.find(distribution_params[:overviewID])
      overview.token = distribution_params[:token]
      overview.status = distribution_params[:status]
      overview.transaction_id = distribution_params[:transactionID]
      overview.transaction_type = distribution_params[:transactionType]
      overview.wallet_address = distribution_params[:walletAddress]
      if overview.save
        channel =
          if overview.user.present?
            "transactions_#{overview.user.id}_channel"
          elsif overview.patient.present?
            "transactions_#{overview.patient.id}_channel"
          end
        if channel.present?
          ActionCable.server.broadcast channel,
                                       overview: overview,
                                       user: overview.user,
                                       patient: overview.patient
          head :ok
        end
      end
    end
  end

  private

  def distribution_params
    params.require(:dashboard).permit(:key, :token, :status, :transactionID, :overviewID, :transactionType, :walletAddress)
  end
end
