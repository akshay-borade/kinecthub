class TransactionsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "transactions_#{params['user_id']}_channel"
  end
end
