module PatientsHelper
  def ethnicity_options
    [['Afro-Caribbean', 'afro_caribbean'], ['Asian', 'asian'], ['Caucasian', 'caucasian'],
     ['Indian', 'indian'], ['Maori/Pacific Islander', 'maori_pacific'], ['Middle East', 'middle_east'],
     ['North Africa', 'north_africa'], ['Southern Europe', 'southern_europe']]
  end

  def all_clinic_doctors
    clinic = current_user.doctor.clinic
    clinic.doctors
  end
end
