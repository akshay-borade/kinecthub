module TreatmentHelper
  def protease_regimens
    ["Darunavir/ritonavir plus tenofovir disoproxil fumarate/emtricitabine", "Darunavir plus tenofovir alafenamide/emtricitabine"]
  end

  def integrase_regimens
    ["Dolutegravir/abacavir/lamivudine", "Dolutegravir plus tenofovir disoproxil fumarate/emtricitabine", "Dolutegravir plus tenofovir alafenamide/emtricitabine",
     "Elvitegravir/cobicistate/tenofovir alafenamide/emtricitabine", "Elvitegravir/cobicistate/tenofovir disoproxil fumarate/emtricitabine",
     "Raltegravir plus tenofovir disoproxil fumarate/emtricitabine", "Raltegravir plus tenofovir alafenamide/emtricitabine"]
  end

  def is_treatment_type_other qsk
    return false if Treatment::OPTION_VALUES.except(:other).keys.include?(@treatment.questions[qsk.to_s]&.to_sym)
    if @treatment.questions[qsk.to_s].blank?
      return !!Treatment::DEFAULT_OPTION[qsk]
    end
    true
  end

  def selected_value qsk
    is_treatment_type_other(qsk) ? 'other' : @treatment.questions[qsk.to_s]
  end
end
