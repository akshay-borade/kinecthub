module ClinicsHelper
  def available_patients clinic
    clinic.all_patients.count - total_patient(clinic)
  end

  def patients_outcomes_status(clinic, status)
    current_user.transactions.where(clinic_id: clinic.id, status: status).map { |t| t.patients_ids }.flatten.count
  end

  def total_patient clinic
    current_user.transactions.where(clinic_id: clinic.id).map { |t| t.patients_ids }.flatten.count
  end
end
