module ConsultationsHelper
  def all_healthcare_professionals
    current_user.clinic.try(:healthcare_professionals)
  end
end
