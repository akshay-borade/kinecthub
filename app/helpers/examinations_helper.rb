module ExaminationsHelper
  def avpu_options
    [['Not Assessed', 'not_assessed'], ['Alert', 'alert'], ['Unresponsive', 'unresponsive'],
     ['Responds to Voice', 'responds_to_voice'], ['Responds to Pain', 'responds_to_pain']]
  end
end
