module LaboratoryResultsHelper

  def laboratory_result_input_class qsk
    input_class = "questions-#{qsk}-input form-control"
    if !test_ordered?(qsk.to_s)
      input_class << " display_none"
    end
    input_class
  end

  def laboratory_result_span_class qsk
    span_class = "questions-#{qsk}-span"
    if test_ordered?(qsk.to_s)
      span_class << " display_none"
    end
    span_class
  end
end
