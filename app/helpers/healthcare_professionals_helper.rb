module HealthcareProfessionalsHelper
  def roles_list
    ['Clinician', 'Doctor', 'Nurse', 'Specialist']
  end
end
