module LaboratoryRequestsHelper
  def test_ordered?(test_name)
    @laboratory_result.laboratory_request&.questions&.dig(test_name) == "true"
  end
end
